FROM python:3
WORKDIR /app

RUN apt-get update -y

COPY requirements.txt ./

RUN pip3 install -r requirements.txt

COPY . ./

RUN chmod a+x *.py
ENTRYPOINT ["python3", "./extract_NE.py"]
