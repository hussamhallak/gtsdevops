from boilerpy3 import extractors
from google_trans_new import google_translator
from collections import Counter
import requests
import stanza
import sys

stanza.download('en')
nlp = stanza.Pipeline(lang='en' , processors='tokenize,ner')

translator = google_translator()
extractor = extractors.ArticleExtractor()

def isValid(url):
	try:
		r = requests.head(url, allow_redirects=True)
		#print(r.status_code)
		return r.status_code
	except (requests.ConnectionError, requests.exceptions.MissingSchema):
		print('URL entered is not a valid URL. Please use http://example.com')
		return False
		
def get_ents(url, translate_api = translator.translate):
	try:
		content = extractor.get_content_from_url(url)
	except:
		print("This URL did not return a status code of 200. Try a different URL.")
		return
	if len(content) > 5000:
		print('The text in this page exceeds 5000 character limit!')
		return
	try:
		output = translator.translate(content, lang_tgt='en')
	except Exception as e:
		print (e)
		print("Google Translate API returned an error")
		return []
	translated_content = output
	doc = nlp(translated_content)
	return doc.ents
	
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print ("Usage: Python gts.py <url>")
		print ("e.g: python gts.py http://example.com")
		sys.exit()
	elif isValid(sys.argv[1]) != 200:
		sys.exit()
	else:
		url = sys.argv[1]
		ents = get_ents(url)
		print(*[f'entity: {ent.text}\ttype: {ent.type}' for ent in ents], sep='\n')
