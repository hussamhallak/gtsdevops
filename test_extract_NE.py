import unittest
from extract_NE import *
from types import SimpleNamespace

class TestExtractNE(unittest.TestCase):
	maxDiff = None
	def test_isValid(self):
		self.assertEqual(isValid("https://www.google.com/"), 200)
		self.assertEqual(isValid('http://google.com'), 200)
		self.assertFalse(isValid("google"))
		self.assertFalse(isValid("google.com"))
		self.assertFalse(isValid(0))
		self.assertFalse(isValid(1))
		self.assertFalse(isValid(-1))
		self.assertFalse(isValid(''))
		self.assertFalse(isValid('Any Random String'))
		self.assertFalse(isValid(True))
		self.assertFalse(isValid('http://google'))		
		self.assertRaises(TypeError, isValid(False))
		self.assertRaises(TypeError, isValid(''))
		self.assertRaises(TypeError, isValid(0))
		self.assertRaises(TypeError, isValid(1))
		self.assertRaises(TypeError, isValid(-1))
		self.assertRaises(TypeError, isValid('A string'))
		self.assertRaises(TypeError, isValid('google'))
		self.assertRaises(TypeError, isValid('google.com'))
		self.assertRaises(TypeError, isValid('http://google'))
		
	def mock_translate_api(self, content):
		return SimpleNamespace(text="Abdullah Al Muhairi 3 comments\nI would like to display some pictures from a group of Arab cities, that's it. Click on the image to reach its page in Flickr, and I have to note that I did not ask anyone to display the pictures here, and I do not have the right to any of these pictures, just admire them.\nI hope to see more pictures from all Arab countries, if you do not participate then please do so now, if time permits, the group is open to everyone and participation is free! All you have to do is upload your photos to Flickr and share them to the group, that's it.\nShare this topic:")
		
		
	def test_get_ents(self):
		ents = [{
  "text": "Abdullah Al Muhairi",
  "type": "PERSON",
  "start_char": 0,
  "end_char": 19
}, {
  "text": "3",
  "type": "CARDINAL",
  "start_char": 20,
  "end_char": 21
}, {
  "text": "Arab",
  "type": "NORP",
  "start_char": 86,
  "end_char": 90
}, {
  "text": "Flickr",
  "type": "ORG",
  "start_char": 150,
  "end_char": 156
}, {
  "text": "Arab",
  "type": "NORP",
  "start_char": 343,
  "end_char": 347
}, {
  "text": "Flickr",
  "type": "ORG",
  "start_char": 526,
  "end_char": 532
}]

		frents = get_ents("https://smallpages.blog/2019/07/27/photos/", self.mock_translate_api)
		for expected, actual in zip(ents, frents):
			self.assertEqual(expected, actual.to_dict())
		
		#output = "'عبدالله المهيري 3 تعليقات\nأود عرض بعض الصور من مجموعة مدن عربية ، هذا كل شيء، اضغط على الصورة لتصل لصفحتها في فليكر، وعلي أن أنوه بأنني لم أستأذن أحداً في عرض الصور هنا، ولست أملك الحق لأي من هذه الصور، فقط معجب بها.\nأتمنى رؤية مزيد من الصور من كل الدول العربية، إن لم تشارك فأرجو أن تفعل ذلك الآن إن سمح الوقت بذلك ، المجموعة مفتوحة للجميع والمشاركة مجانية! كل ما عليك فعله هو رفع صورك إلى فليكر والمشاركة بها في المجموعة، هذا كل شيء.\nشارك هذا الموضوع:\n'	
