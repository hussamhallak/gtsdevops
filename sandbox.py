from boilerpy3 import extractors
from googletrans import Translator
from collections import Counter
import requests
import stanza
import sys
nlp = stanza.Pipeline(lang='en' , processors='tokenize,ner')

translator = Translator()
extractor = extractors.ArticleExtractor()

def isValid(url):
	try:
		r = requests.head(url, allow_redirects=True)
		#print(r.status_code)
		return r.status_code
	except (requests.ConnectionError, requests.exceptions.MissingSchema):
		print('URL entered is not a valid URL. Please use http://example.com')
		return False
		
def get_content_from_url(url):
	try:
		content = extractor.get_content_from_url(url)
		print(content)
		return content
	except:
		print("This URL did not return a status code of 200. Try a different URL.")
		return
		
def translate_content(content):
	if len(content) > 5000:
		print('The text in this page exceeds 5000 character limit!')
		return
	else:
		output = translator.translate(content)
		translated_content = output.text
		return translated_content	


def extract_nes(translated_content):	

	doc = nlp(translated_content)
	return doc.ents
	

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print ("Usage: Python gts.py <url>")
		print ("e.g: python gts.py http://example.com")
		sys.exit()
	elif isValid(sys.argv[1]) != 200:
		sys.exit()
	else:
		url = sys.argv[1]
		content = get_content_from_url(url)
		#translated_content = translate_content(content)
		#ents = extract_nes(translated_content)
		#print(*[f'entity: {ent.text}\ttype: {ent.type}' for ent in ents], sep='\n')
