import subprocess
import sys

if __name__ == "__main__":
	output = subprocess.check_output(["python3 extract_NE.py https://smallpages.blog/2019/07/27/photos/ 2> /dev/null"], shell=True)
	expected = b"""entity: Abdullah Al Muhairi	type: PERSON
entity: 3	type: CARDINAL
entity: Arab	type: NORP
entity: Flickr	type: ORG
entity: Arab	type: NORP
entity: Flickr	type: ORG
"""
	expectederr = b"Google Translate API returned an error\n\n"

	if output not in [expected, expectederr]:
		print("FAILED Integration Test!")
		print(output)
		sys.exit()
		
	print("Integration Test Succeeded!")